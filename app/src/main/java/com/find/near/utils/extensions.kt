package com.binaa.ebook.utils

import android.view.View
import android.widget.EditText
import android.widget.TextView

/**
 * Created by Sanuraj on 03/04/18.
 */

fun View.setValueToView(value: String) {
    if (this is TextView) {
        this.text = value
    } else if (this is EditText) {
        this.setText(value)
    }
}

fun View.getValueFromView(): String? {
    if (this is TextView) {
        return this.text.toString()
    } else if (this is EditText) {
        return this.text.toString()
    }
    return null
}



var TextView.textColor: String
    get() {
        return "";
    }
    set(value: String) {
        this.setTextColor(android.graphics.Color.parseColor(value))
    }

fun <T : Any, R> whenNotNull(input: T?, callback: (T) -> R): R? {
    return input?.let(callback)
}




