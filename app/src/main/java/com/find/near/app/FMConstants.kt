package com.find.near.app

class FMConstants {
    companion object {
        const val PLACE_KEY = "AIzaSyAhFKpsCKxJHeEejneU9OK3YeKqFl4jDqA"
        const val IMAGE_REFERENCE: String = "imageUrl"
        const val IMAGE_WIDTH: String = "imageWidth"
        const val IMAGE_HEIGHT: String = "imageHeight"

        const val PHOTO_URL = "https://maps.googleapis.com/maps/api/place/photo?photoreference=@@&maxheight=**&maxwidth=%%&sensor=true&key=" + PLACE_KEY
        const val TYPE = "type"
    }
}