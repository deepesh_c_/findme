package com.find.near.app

import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication

class FMApplication : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()
        MultiDex.install(this)
    }
}