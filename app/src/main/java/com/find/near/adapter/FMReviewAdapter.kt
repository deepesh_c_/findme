package com.binaa.ebook.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.bumptech.glide.Glide
import com.find.near.R
import com.find.near.api.response.Review
import kotlinx.android.synthetic.main.review_list_adapter.view.*

/**
 * Created by Sanuraj on 28/03/18.
 */

class FMReviewAdapter(val context: Context, val reviewList: List<Review>) : RecyclerView.Adapter<FMReviewAdapter.ViewHolder>() {

    internal var onItemClickListener: OnItemClickListener? = null

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener) {
        this.onItemClickListener = onItemClickListener

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.review_list_adapter, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val result = reviewList.get(position) as Review
        holder.name.text = result.authorName
        holder.review.text = result.text
        Glide.with(context).load(result.profilePhotoUrl).into(holder.userImage)
        if (result.rating == 0) {
            holder.ratingBar.visibility = View.GONE
        } else {
            holder.ratingBar.visibility = View.VISIBLE
            val rate: Float = result.rating.toFloat()
            holder.ratingBar.rating = rate
        }

    }

    override fun getItemCount(): Int {
        return reviewList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name = itemView.userName as TextView
        val review = itemView.review as TextView
        val userImage = itemView.userImage as ImageView
        val ratingBar = itemView.ratingBar as RatingBar
        val view = itemView
    }
}
