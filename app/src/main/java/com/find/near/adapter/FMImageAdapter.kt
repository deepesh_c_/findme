package com.find.near.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.find.near.api.response.Photo
import com.find.near.fragment.FMImageFragment

class FMImageAdapter(fragmentManager: FragmentManager, val photos: List<Photo>) :
        FragmentStatePagerAdapter(fragmentManager) {

    // 2
    override fun getItem(position: Int): Fragment {
        return FMImageFragment.newInstance(photos.get(position))
    }

    // 3
    override fun getCount(): Int {
        return photos.size
    }
}