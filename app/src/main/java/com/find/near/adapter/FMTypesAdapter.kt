package com.binaa.ebook.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.find.near.R
import com.find.near.models.FMTypes
import kotlinx.android.synthetic.main.type_list_adapter.view.*

/**
 * Created by Sanuraj on 28/03/18.
 */

class FMTypesAdapter(val context: Context?, val tyeList: List<FMTypes>?) : RecyclerView.Adapter<FMTypesAdapter.ViewHolder>() {

    internal var onItemClickListener: OnItemClickListener? = null

    interface OnItemClickListener {
        fun onItemClick(type: FMTypes)
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener) {
        this.onItemClickListener = onItemClickListener

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.type_list_adapter, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val result = tyeList!!.get(position) as FMTypes
        holder.type.text = result.name
        holder.type.tag = result
        holder.typeImage.tag = result
        holder.typeImage.setOnClickListener(View.OnClickListener { v ->
            onItemClickListener!!.onItemClick(v.tag as FMTypes)
        })
        holder.type.setOnClickListener(View.OnClickListener { v ->
            onItemClickListener!!.onItemClick(v.tag as FMTypes)
        })
    }

    override fun getItemCount(): Int {
        return tyeList!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val type = itemView.type as TextView
        val typeImage = itemView.typeImage as ImageView
        val view = itemView
    }
}
