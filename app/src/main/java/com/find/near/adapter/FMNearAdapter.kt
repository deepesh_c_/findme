package com.binaa.ebook.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.TextView
import com.find.near.R
import com.find.near.api.response.Result
import kotlinx.android.synthetic.main.near_list_adapter.view.*

/**
 * Created by Sanuraj on 28/03/18.
 */

class FMNearAdapter(val context: Context, val resultList: List<Result>) : RecyclerView.Adapter<FMNearAdapter.ViewHolder>() {

    internal var onItemClickListener: OnItemClickListener? = null

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener) {
        this.onItemClickListener = onItemClickListener

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.near_list_adapter, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val result = resultList.get(position) as Result
        holder.name.text = result.name
        holder.place.text = result.vicinity
        holder.openHrs.text = "9 AM - 10 PM"
        if (result.rating == 0.0) {
            holder.rating.visibility = View.GONE
            holder.ratingBar.visibility = View.GONE
        } else {
            holder.rating.visibility = View.VISIBLE
            holder.ratingBar.visibility = View.VISIBLE
            holder.rating.text = result.rating.toString()
            val rate: Float = result.rating.toFloat()
            holder.ratingBar.rating = rate
        }
        holder.view.setOnClickListener(View.OnClickListener { v ->
            onItemClickListener?.onItemClick(position)
        })
    }

    override fun getItemCount(): Int {
        return resultList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name = itemView.name as TextView
        val place = itemView.place as TextView
        val openHrs = itemView.opening_hours as TextView
        val rating = itemView.rating as TextView
        val ratingBar = itemView.rating_bar as RatingBar
        val view = itemView
    }
}
