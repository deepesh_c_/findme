package com.find.near.api.response

import com.squareup.moshi.Json


data class FMPlaceResponse(
        @Json(name = "html_attributions") val htmlAttributions: List<Any>,
        @Json(name = "results") val results: List<Result>,
        @Json(name = "status") val status: String
) {
    val isEmptyResult get() = results.isEmpty()
}

data class FNPlaceDetails(
        @Json(name = "html_attributions") val htmlAttributions: List<Any>,
        @Json(name = "result") val result: Result,
        @Json(name = "status") val status: String
)

data class Result(
        @Json(name = "address_components") val addressComponents: List<AddressComponent>,
        @Json(name = "adr_address") val adrAddress: String,
        @Json(name = "formatted_address") val formattedAddress: String,
        @Json(name = "formatted_phone_number") val formattedPhoneNumber: String,
        @Json(name = "geometry") val geometry: Geometry,
        @Json(name = "icon") val icon: String,
        @Json(name = "id") val id: String,
        @Json(name = "international_phone_number") val internationalPhoneNumber: String,
        @Json(name = "name") val name: String,
        @Json(name = "opening_hours") val openingHours: OpeningHours,
        @Json(name = "photos") val photos: List<Photo>,
        @Json(name = "place_id") val placeId: String,
        @Json(name = "rating") val rating: Double,
        @Json(name = "reference") val reference: String,
        @Json(name = "reviews") val reviews: List<Review>,
        @Json(name = "scope") val scope: String,
        @Json(name = "types") val types: List<String>,
        @Json(name = "url") val url: String,
        @Json(name = "utc_offset") val utcOffset: Int,
        @Json(name = "vicinity") val vicinity: String,
        @Json(name = "website") val website: String
) {
    val isPhotoEmpty get() = photos.size
}

data class Review(
        @Json(name = "author_name") val authorName: String,
        @Json(name = "author_url") val authorUrl: String,
        @Json(name = "language") val language: String,
        @Json(name = "profile_photo_url") val profilePhotoUrl: String,
        @Json(name = "rating") val rating: Int,
        @Json(name = "relative_time_description") val relativeTimeDescription: String,
        @Json(name = "text") val text: String,
        @Json(name = "time") val time: Int
)

data class Geometry(
        @Json(name = "location") val location: Location,
        @Json(name = "viewport") val viewport: Viewport
)

data class Viewport(
        @Json(name = "northeast") val northeast: Northeast,
        @Json(name = "southwest") val southwest: Southwest
)

data class Southwest(
        @Json(name = "lat") val lat: Double,
        @Json(name = "lng") val lng: Double
)

data class Northeast(
        @Json(name = "lat") val lat: Double,
        @Json(name = "lng") val lng: Double
)

data class Location(
        @Json(name = "lat") val lat: Double,
        @Json(name = "lng") val lng: Double
)

data class Photo(
        @Json(name = "height") val height: Int,
        @Json(name = "html_attributions") val htmlAttributions: List<String>,
        @Json(name = "photo_reference") val photoReference: String,
        @Json(name = "width") val width: Int
)

data class AddressComponent(
        @Json(name = "long_name") val longName: String,
        @Json(name = "short_name") val shortName: String,
        @Json(name = "types") val types: List<String>
)

data class OpeningHours(
        @Json(name = "open_now") val openNow: Boolean,
        @Json(name = "periods") val periods: List<Period>,
        @Json(name = "weekday_text") val weekdayText: List<String>
)

data class Period(
        @Json(name = "open") val open: Open
)

data class Open(
        @Json(name = "day") val day: Int,
        @Json(name = "time") val time: String
)