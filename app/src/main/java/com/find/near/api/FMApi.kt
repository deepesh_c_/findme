package com.news.read.api

import com.find.near.api.response.FMPlaceResponse
import com.find.near.api.response.FMTypeResponse
import com.find.near.api.response.FNPlaceDetails
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface FMApi {

    @GET("bins/de9pz")
    fun getCategory(): Call<FMTypeResponse>;

    @GET("maps/api/place/nearbysearch/json")
    fun getNearPlaces(@Query("location") location: String, @Query("radius") radius: String, @Query("types") types: String?, @Query("key") key: String)
            : Call<FMPlaceResponse>;

    @GET("maps/api/place/details/json")
    fun getDetails(@Query("reference") reference: String, @Query("sensor") sensor: Boolean, @Query("key") key: String)
            : Call<FNPlaceDetails>;

}