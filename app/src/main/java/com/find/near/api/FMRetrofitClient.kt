package com.news.read.api

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object FMRetrofitClient {
    fun create(): FMApi {
        val logInterceptor = HttpLoggingInterceptor()
        logInterceptor.level = HttpLoggingInterceptor.Level.BODY;

        val client = OkHttpClient.Builder()
                .addInterceptor(logInterceptor)
                .build()
        val retrofit = Retrofit.Builder()
                .baseUrl("https://maps.googleapis.com")
                .addConverterFactory(MoshiConverterFactory.create()).client(client)
                .build()

        return retrofit.create<FMApi>(FMApi::class.java!!)
    }

    fun createType(): FMApi {
        val logInterceptor = HttpLoggingInterceptor()
        logInterceptor.level = HttpLoggingInterceptor.Level.BODY;

        val client = OkHttpClient.Builder()
                .addInterceptor(logInterceptor)
                .build()
        val retrofit = Retrofit.Builder()
                .baseUrl("https://api.myjson.com")
                .addConverterFactory(MoshiConverterFactory.create()).client(client)
                .build()

        return retrofit.create<FMApi>(FMApi::class.java!!)
    }
}