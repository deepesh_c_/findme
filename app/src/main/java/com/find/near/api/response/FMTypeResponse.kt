package com.find.near.api.response

import com.find.near.models.FMTypes
import com.squareup.moshi.Json


data class FMTypeResponse(
        @Json(name = "status") val status: String,
        @Json(name = "data") val data: List<FMTypes>
) {
    val isEmpty get() = data.isEmpty()
}

