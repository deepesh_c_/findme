package com.find.near.models

import com.squareup.moshi.Json


data class FMTypes(
        @Json(name = "type") val type: String,
        @Json(name = "name") val name: String
)
