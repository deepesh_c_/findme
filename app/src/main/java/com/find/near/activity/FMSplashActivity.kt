package com.find.near.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.find.near.R

class FMSplashActivity : FMBaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fm_splash)
        init() 
    }      
    private fun init() {
        Handler().postDelayed({
            finish()
            startActivity(Intent(this@FMSplashActivity, FMHomeActivity::class.java))
        }, 3000)
    }
	    
}
