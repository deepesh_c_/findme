package com.find.near.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.view.View
import com.binaa.ebook.adapter.FMTypesAdapter
import com.binaa.ebook.utils.whenNotNull
import com.find.near.R
import com.find.near.api.response.FMTypeResponse
import com.find.near.app.FMConstants
import com.find.near.models.FMTypes
import com.news.read.api.FMRetrofitClient
import kotlinx.android.synthetic.main.activity_fm_home.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FMHomeActivity : FMBaseActivity() {
    private var context: Context? = null
    private var typeAdapter: FMTypesAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fm_home)
        context = this;
        initViews()
    }

    private fun initViews() {
        placeTypeGrid.layoutManager = GridLayoutManager(context, 3)
        placeTypeGrid.setHasFixedSize(true)
        getTypes();
    }


    fun getTypes() {
        val placeApi = FMRetrofitClient.createType()
        val places = placeApi.getCategory();
        indicator.visibility = View.VISIBLE
        places.enqueue(object : Callback<FMTypeResponse> {
            override fun onResponse(call: Call<FMTypeResponse>?, response: Response<FMTypeResponse>?) {
                indicator.visibility = View.GONE

                whenNotNull(response!!.body()) {
                    whenNotNull(it.data) {
                        if (!it.isEmpty()) {
                            typeAdapter = FMTypesAdapter(context, it)
                            typeAdapter!!.setOnItemClickListener(object : FMTypesAdapter.OnItemClickListener {
                                override fun onItemClick(type: FMTypes) {

                                    val intent = Intent(context, FMListActivity::class.java)
                                    intent.putExtra(FMConstants.TYPE, type.type)
                                    startActivity(intent)
                                }
                            })
                            placeTypeGrid.adapter = typeAdapter
                        }

                        it.forEach {

                            Log.d("NAME", "NAME = " + it.name);
                        }

                    }

                }
            }


            override fun onFailure(call: Call<FMTypeResponse>?, t: Throwable?) {
                Log.d("", "" + t);
            }
        })
    }
}