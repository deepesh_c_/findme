package com.find.near.activity

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import com.beust.klaxon.*
import com.binaa.ebook.adapter.FMNearAdapter
import com.binaa.ebook.utils.whenNotNull
import com.find.near.R
import com.find.near.api.response.FMPlaceResponse
import com.find.near.api.response.Result
import com.find.near.app.FMConstants
import com.find.near.location.FMLocationManager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.news.read.api.FMRetrofitClient
import kotlinx.android.synthetic.main.activity_fm_list.*
import org.jetbrains.anko.async
import org.jetbrains.anko.uiThread
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.URL


class FMListActivity : AppCompatActivity(), OnMapReadyCallback, FMLocationManager.FMLocationUpdate, GoogleMap.OnCameraIdleListener, GoogleMap.OnCameraMoveListener {


    var isListed: Boolean = false
    private lateinit var mMap: GoogleMap
    lateinit var locationManager: FMLocationManager
    private var neardapter: FMNearAdapter? = null
    private var currentMarker: Marker? = null
    private var currentLocation: Location? = null
    private var context: Context? = null
    private var height: Int? = 0;
    private var behavior: BottomSheetBehavior<LinearLayout>? = null
    private var type: String? = null

    override fun setLocation(location: Location) {
        currentLocation = location
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(location.latitude, location.longitude), 15f));
        getPlaceList(location)
        if (null == currentMarker) {
            val icon = BitmapDescriptorFactory.fromResource(R.drawable.current_position)
            currentMarker = mMap.addMarker(MarkerOptions().position(LatLng(location.latitude, location.longitude)).icon(icon))
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getIntentData()
        setContentView(R.layout.activity_fm_list)
        context = this
        initViews()
    }

    private fun getIntentData() {
        type = intent.getStringExtra(FMConstants.TYPE);
    }

    private fun initViews() {
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        // The View with the BottomSheetBehavior
        val bottomSheet = bottom_sheet
        behavior = BottomSheetBehavior.from(bottomSheet)
        behavior!!.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                // React to state change
                Log.e("onStateChanged", "onStateChanged:" + newState)
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                // React to dragging events
                Log.e("onSlide", "onSlide")
            }
        })
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        height = displayMetrics.heightPixels
        behavior!!.peekHeight = height!! / 2
        val layoutManager = LinearLayoutManager(this)
        nearList.layoutManager = layoutManager

    }

    override fun onCameraIdle() {
        behavior!!.peekHeight = height!! / 2
    }

    override fun onCameraMove() {
        behavior!!.peekHeight = 0
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.setOnCameraIdleListener(this)
        mMap.setOnCameraMoveListener(this)
        setupPermissions();

    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            1000 -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {

                    locationManager = FMLocationManager(this)
                    locationManager.connect(this)
                }
            }
        }
    }


    private fun setupPermissions() {
        val permissionFineLocation = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
        val permissionCoarseLocation = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)

        if (permissionCoarseLocation != PackageManager.PERMISSION_GRANTED && permissionFineLocation != PackageManager.PERMISSION_GRANTED) {
            makeRequest()
        } else {
            locationManager = FMLocationManager(this)
            locationManager.connect(this)
        }
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION),
                1000)
    }

    fun connectLocation() {

    }

    private fun getPlaceList(location: Location) {
        if (!isListed) {
            indicator.visibility = View.VISIBLE
            isListed = true
            val placeApi = FMRetrofitClient.create()

            val places = placeApi.getNearPlaces("" + location.latitude + "," + location.longitude, "5000", type, FMConstants.PLACE_KEY);
            places.enqueue(object : Callback<FMPlaceResponse> {
                override fun onResponse(call: Call<FMPlaceResponse>?, response: Response<FMPlaceResponse>?) {
                    indicator.visibility = View.GONE

                    whenNotNull(response!!.body()) {
                        if (!it.isEmptyResult) {
                            setNearList(it.results)
                            setMapView(it.results)
                        }
                    }
                }


                override fun onFailure(call: Call<FMPlaceResponse>?, t: Throwable?) {
                    Log.d("", "" + t);
                }
            })
        }
    }

    private fun setNearList(resultList: List<Result>) {
        neardapter = FMNearAdapter(this, resultList)
        neardapter!!.setOnItemClickListener(object : FMNearAdapter.OnItemClickListener {
            override fun onItemClick(position: Int) {
                val intent = Intent(context, FMPlaceDetailsActivity::class.java)
                intent.putExtra("DETAILS_REFERENCE", resultList.get(position).reference)
                startActivity(intent)
            }
        })
        nearList.adapter = neardapter
//        calculateNearPlace(resultList)
    }

    private fun setMapView(resultList: List<Result>) {
        var bounds = LatLngBounds.builder()
        resultList.forEach {
            val myPlace = LatLng(it.geometry.location.lat, it.geometry.location.lng)  // this is New York
            mMap.addMarker(MarkerOptions().position(myPlace).title(it.name))
            bounds.include(myPlace)
        }
        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 50));
    }

    private fun calculateNearPlace(resultList: List<Result>) {
        var nearData: Float
        var prevData: Float? = null
        var nearDt: Result? = null
        resultList.forEach {
            val dest = Location("")
            dest.latitude = it.geometry.location.lat
            dest.longitude = it.geometry.location.lng
            nearData = currentLocation!!.distanceTo(dest)
            if (null != prevData) {
                if (nearData.compareTo(prevData!!) == -1) {
                    nearDt = it
                    prevData = nearData
                }
            } else {
                prevData = nearData
            }
        }
        setDrawNearRoute(currentLocation, nearDt?.geometry?.location)
    }

    fun setDrawNearRoute(source: Location?, destination: com.find.near.api.response.Location?) {
        val options = PolylineOptions()
        options.color(Color.RED)
        options.width(5f)
        val url = getURL(LatLng(source!!.latitude, source!!.longitude), LatLng(destination!!.lat, destination!!.lng))
        async {
            val result = URL(url).readText()
            var a: JsonArray<JsonObject>? = null
            uiThread {
                val parser: Parser = Parser()
                val stringBuilder: StringBuilder = StringBuilder(result)
                val json: JsonObject = parser.parse(stringBuilder) as JsonObject
                val routes = json.array<JsonObject>("routes")
                routes?.forEach {
                    val t = it.array<JsonObject>("legs")
                    t?.forEach {
                        a = it.array<JsonObject>("steps")

                    }
                }
                val points = a!![0]
                val polypts = points.flatMap { decodePoly(points.obj("polyline")?.string("points")!!) }
                options.add(LatLng(source!!.latitude, source!!.longitude))
                for (point in polypts) options.add(point)
                options.add(LatLng(destination!!.lat, destination!!.lng))
                mMap!!.addPolyline(options)
            }

        }
    }

    /**
     * Method to decode polyline points
     * Courtesy : https://jeffreysambells.com/2010/05/27/decoding-polylines-from-google-maps-direction-api-with-java
     */
    private fun decodePoly(encoded: String): List<LatLng> {
        val poly = ArrayList<LatLng>()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0

        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat

            shift = 0
            result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng

            val p = LatLng(lat.toDouble() / 1E5,
                    lng.toDouble() / 1E5)
            poly.add(p)
        }

        return poly
    }

    fun getURL(from: LatLng, to: LatLng): String {
        val origin = "origin=" + from.latitude + "," + from.longitude
        val dest = "destination=" + to.latitude + "," + to.longitude
        val sensor = "sensor=false"
        val params = "$origin&$dest&$sensor"
        return "https://maps.googleapis.com/maps/api/directions/json?$params"
    }
}