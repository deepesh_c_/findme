package com.find.near.activity

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import com.binaa.ebook.adapter.FMReviewAdapter
import com.binaa.ebook.utils.whenNotNull
import com.find.near.R
import com.find.near.adapter.FMImageAdapter
import com.find.near.api.response.FNPlaceDetails
import com.find.near.api.response.Result
import com.find.near.app.FMConstants
import com.news.read.api.FMRetrofitClient
import kotlinx.android.synthetic.main.activity_fm_details.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class FMPlaceDetailsActivity : AppCompatActivity() {
    private var reference: String? = null
    private var pagerAdapter: FMImageAdapter? = null
    private var context: Context? = null
    private var reviewAdapter: FMReviewAdapter? = null
    private var result: Result? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        reference = intent.getStringExtra("DETAILS_REFERENCE")
        setContentView(R.layout.activity_fm_details)
        context = this
        initViews()
    }

    private fun initViews() {
        val layoutManager = LinearLayoutManager(this)
        reviewList.layoutManager = layoutManager
        reviewList.isNestedScrollingEnabled = false
        getPlaceList(reference)
        navigate.setOnClickListener(View.OnClickListener { v: View? ->
            whenNotNull(result) {
                val intent = Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?" +
                                "&daddr=" + result?.geometry?.location?.lat + ","
                                + result?.geometry?.location?.lng))
                startActivity(intent)
            }

        })
        sms.setOnClickListener(View.OnClickListener { v -> sms() })
        call.setOnClickListener(View.OnClickListener { v -> call() })
    }

    private fun call() {
//        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + result!!.internationalPhoneNumber))
//        startActivity(intent)
    }

    private fun sms() {

    }

    private fun getPlaceList(reference: String?) {
        indicator.visibility = View.VISIBLE
        val placeApi = FMRetrofitClient.create()

        val places = placeApi.getDetails(reference!!, true, FMConstants.PLACE_KEY);
        places.enqueue(object : Callback<FNPlaceDetails> {
            override fun onResponse(call: Call<FNPlaceDetails>?, response: Response<FNPlaceDetails>?) {
                indicator.visibility = View.GONE

                whenNotNull(response!!.body()) {
                    whenNotNull(it.result) {
                        result = it
                        whenNotNull(it.photos) {
                            if (!it.isEmpty()) {
                                viewPager.visibility = View.VISIBLE
                                noImage.visibility = View.GONE
                                pagerAdapter = FMImageAdapter(supportFragmentManager, it);
                                viewPager.adapter = pagerAdapter
                                viewPager.offscreenPageLimit = it.size + 1
                            } else {
                                noImage.visibility = View.VISIBLE
                                viewPager.visibility = View.GONE
                            }
                        }

                        setValue(it)
                    }

                }
            }


            override fun onFailure(call: Call<FNPlaceDetails>?, t: Throwable?) {
                Log.d("", "" + t);
            }
        })
    }

    fun setValue(result: Result) {
        name.text = result.name
        place.text = result.formattedAddress
        contact.text = result.internationalPhoneNumber
        var openHrs: String? = ""
        whenNotNull(result.openingHours) {
            whenNotNull(it.weekdayText) {
                it.forEach {
                    openHrs = openHrs + it + System.lineSeparator()
                }
            }
        }

        openingHours.text = openHrs
        if (result.rating == 0.0) {
            rating.visibility = View.GONE
            ratingBar.visibility = View.GONE
        } else {
            rating.visibility = View.VISIBLE
            ratingBar.visibility = View.VISIBLE
            rating.text = result.rating.toString()
            val rate: Float = result.rating.toFloat()
            ratingBar.rating = rate
        }
        whenNotNull(result.reviews) {
            reviewAdapter = FMReviewAdapter(this, it)
            reviewAdapter!!.setOnItemClickListener(object : FMReviewAdapter.OnItemClickListener {
                override fun onItemClick(position: Int) {

                }
            })
            reviewList.adapter = reviewAdapter
        }
        nestedScroll.visibility = View.VISIBLE

    }
}