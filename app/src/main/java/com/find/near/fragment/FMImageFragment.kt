package com.find.near.fragment

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.find.near.R
import com.find.near.api.response.Photo
import com.find.near.app.FMConstants
import kotlinx.android.synthetic.main.pager_item.view.*
import org.jetbrains.anko.async
import org.jetbrains.anko.imageBitmap
import org.jetbrains.anko.uiThread
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

class FMImageFragment : Fragment() {
    var image: Bitmap? = null


    //2
    companion object {
        fun newInstance(photo: Photo): FMImageFragment {
            val args = Bundle()
            args.putString(FMConstants.IMAGE_REFERENCE, photo.photoReference)
            args.putInt(FMConstants.IMAGE_WIDTH, photo.width)
            args.putInt(FMConstants.IMAGE_HEIGHT, photo.height)
            val fragment = FMImageFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.pager_item, container, false)
        val imageView = view.itemImage as ImageView
        val args = arguments
        var photoUrl = FMConstants.PHOTO_URL.replace("@@", args?.getString(FMConstants.IMAGE_REFERENCE).toString())
        photoUrl = photoUrl.replace("**", "" + args?.getInt(FMConstants.IMAGE_HEIGHT));
        photoUrl = photoUrl.replace("%%", "" + args?.getInt(FMConstants.IMAGE_WIDTH));

        async {
            try {
                val url = URL(photoUrl)
                val connection = url.openConnection() as HttpURLConnection
                connection.doOutput = true
                connection.connect()
                val input = connection.inputStream as InputStream
                image = BitmapFactory.decodeStream(input)
                Log.d("", "" + image);

            } catch (e: IOException) {

            }
            uiThread {
                imageView.imageBitmap = image
            }

        }
        return view
    }

}
